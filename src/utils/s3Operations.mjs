import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import s3Client from '../config/s3Config.mjs';
import { PutObjectCommand, GetObjectCommand } from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';

// Utility to handle __dirname and __filename with ES6 modules
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const expiresIn7Days = 60 * 60 * 24 * 7;

export const uploadFile = async (fileName, bucket) => {
  const fileContent = fs.readFileSync(path.join(__dirname, '../resources/images', fileName));
  const params = {
    Bucket: bucket,
    Key: path.basename(fileName),
    Body: fileContent,
  };

  const command = new PutObjectCommand(params);
  return s3Client.send(command);
};

export const generatePresignedUrl = async (bucket, key) => {
  const params = {
    Bucket: bucket,
    Key: key,
  };

  const command = new GetObjectCommand(params);
  return getSignedUrl(s3Client, command, { expiresIn: expiresIn7Days });
};
