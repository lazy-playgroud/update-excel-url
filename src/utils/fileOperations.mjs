import XLSX from 'xlsx';
import path from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';

// Utility to handle __dirname and __filename with ES6 modules
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const loadExcelFile = (filePath) => {
  const workbook = XLSX.readFile(path.join(__dirname, '../resources', filePath));
  const sheetName = workbook.SheetNames[0];
  const sheet = workbook.Sheets[sheetName];
  return XLSX.utils.sheet_to_json(sheet);
};

export const saveExcelFile = (data, filePath) => {
  const newSheet = XLSX.utils.json_to_sheet(data);
  const newWorkbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(newWorkbook, newSheet, 'Sheet1');
  XLSX.writeFile(newWorkbook, path.join(__dirname, '../resources', filePath));
};
