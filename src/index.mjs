import { loadExcelFile, saveExcelFile } from './utils/fileOperations.mjs';
import { uploadFile, generatePresignedUrl } from './utils/s3Operations.mjs';
import path from 'path';

const bucketName = '170913876433-deployments';
const main = async () => {
  const data = loadExcelFile('InsurancePlan.xlsx');
  let counter = 0;
  const totalFiles = data.length;

  const promises = data.map(async (row) => {
    const fileName = row.fileName;
    await uploadFile(fileName, bucketName);
    const presignedUrl = await generatePresignedUrl(bucketName, path.basename(fileName));
    row.presigned_url = presignedUrl;
    counter++;
    console.log(`Processed ${counter}/${totalFiles}: ${fileName}`);
    return row;
  });

  Promise.all(promises)
    .then((updatedData) => {
      saveExcelFile(updatedData, 'InsurancePlanWithUrls.xlsx');
      console.log('Excel file updated with presigned URLs.');
    })
    .catch((error) => {
      console.error('Error:', error);
    });
};

main();
