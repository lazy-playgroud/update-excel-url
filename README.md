# Project Structure
```
project-root/
│
├── src/
│ ├── index.js
│ ├── config/
│ │ └── s3Config.js
│ ├── utils/
│ │ ├── fileOperations.js
│ │ └── s3Operations.js
│ └── resources/
│ ├── images/
│ │ └── (your image files)
│ └── file_list.xlsx
│
├── scripts/
│ ├── upload_images.bat
│ └── upload_images.sh
│
├── package.json
└── package-lock.json
```